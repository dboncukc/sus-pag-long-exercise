#!/bin/bash

for par_name in "mu_eta" "tau_eta" "mu_phi" "tau_phi" "mu_pt" "tau_pt" "mu_m" "tau_m" "mu_mt" "tau_mt" "mu_tau_mt"
do
    echo ".:::::" + $par_name + ":::::."
    python3 newPlot.py $par_name
done