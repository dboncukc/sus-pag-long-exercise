# CHANGE replace with the location of your ntuples
set -e
./Make.sh FinalSelection_mutau.cc
set -e
./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/DY_postEE.root output_mutau_2022/DY_postEE.root DY DY
set -e
./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/TTTo2L2Nu_postEE.root output_mutau_2022/TTTo2L2Nu_postEE.root TTTo2L2Nu TTTo2L2Nu
set -e
./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/WW_postEE.root output_mutau_2022/WW_postEE.root WW WW
set -e
./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/stau_left.root output_mutau_2022/stau_left_postEE.root stau_left stau_left
set -e
./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/stau_right.root output_mutau_2022/stau_right_postEE.root stau_right stau_right
set -e
./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/Muon2022.root output_mutau_2022/Data.root data_obs data_obs
# ./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/Muon2022F.root output_mutau_2022/Muon2022F.root data_obs data_obs
# ./FinalSelection_mutau.exe 2022postEE /eos/user/c/cmsdas/2024/long_ex_sus/ntuples_mutau_2022/Muon2022G.root output_mutau_2022/Muon2022G.root data_obs data_obs
# hadd -f output_mutau_2022/Data.root output_mutau_2022/Muon2022E.root output_mutau_2022/Muon2022F.root output_mutau_2022/Muon2022G.root
set -e
python3 Create_fake.py
set -e
hadd -f datacard_mutau.root output_mutau_2022/DY_postEE.root output_mutau_2022/TTTo2L2Nu_postEE.root output_mutau_2022/WW_postEE.root output_mutau_2022/Data.root output_mutau_2022/Fake.root output_mutau_2022/stau_left_postEE.root output_mutau_2022/stau_right_postEE.root 
set -e
python3 Draw_mutau.py
# set -e
# ./parameter_plots.sh