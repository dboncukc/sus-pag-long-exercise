#!/usr/bin/env python
import ROOT
import re
import argparse
from array import array

ROOT.gROOT.SetBatch(True)


def add_lumi():
    lowX=0.5
    lowY=0.835
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.30, lowY+0.16, "NDC")
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.SetTextSize(0.05)
    lumi.SetTextFont (   42 )
    lumi.AddText("26.7 fb^{-1} (13.6 TeV)")
    return lumi

def add_CMS():
    lowX=0.21
    lowY=0.70
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(61)
    lumi.SetTextSize(0.08)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("CMS")
    return lumi

def add_Preliminary():
    lowX=0.21
    lowY=0.63
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    lumi.SetTextFont(52)
    lumi.SetTextSize(0.06)
    lumi.SetBorderSize(   0 )
    lumi.SetFillStyle(    0 )
    lumi.SetTextAlign(   12 )
    lumi.SetTextColor(    1 )
    lumi.AddText("Internal")
    return lumi

def make_legend():
        output = ROOT.TLegend(0.5, 0.65, 0.92, 0.86, "", "brNDC")
        output.SetNColumns(2)
        output.SetLineWidth(0)
        output.SetLineStyle(0)
        output.SetFillStyle(0)
        output.SetBorderSize(0)
        output.SetTextFont(62)
        return output


ROOT.gStyle.SetOptStat(0)
adapt=ROOT.gROOT.GetColor(12)
new_idx=ROOT.gROOT.GetListOfColors().GetSize() + 1
trans=ROOT.TColor(new_idx, adapt.GetRed(), adapt.GetGreen(),adapt.GetBlue(), "",0.5)


c=ROOT.TCanvas("canvas","",0,0,800,800)
c.cd()


# parameter_name = "tau_pt"
# parameter_title = "Leading p_{T}^{#tau} [GeV]"

parameter_name = "mu_pt"
parameter_title = "Leading p_{T}^{#mu} [GeV]"

# parameter_name = "h_met_pt"
# parameter_title = "MET [GeV]"
signal_scale = 20.0

file=ROOT.TFile("datacard_mutau.root","r")

TT=file.Get("parameters").Get(f"{parameter_name}_TTTo2L2Nu")
VV=file.Get("parameters").Get(f"{parameter_name}_WW")
DY=file.Get("parameters").Get(f"{parameter_name}_DY")
STL = file.Get("parameters").Get(f"{parameter_name}_stau_left")
STL.Scale(signal_scale)

samples = [TT, VV, DY, STL]
for s in samples:
    content_last = s.GetBinContent(s.GetNbinsX())
    error_last = s.GetBinError(s.GetNbinsX())
    content_over = s.GetBinContent(s.GetNbinsX()+1)
    error_over = s.GetBinContent(s.GetNbinsX()+1)
    s.SetBinContent( s.GetNbinsX(), content_last+content_over )
    s.SetBinError( s.GetNbinsX(), ROOT.sqrt(error_last*error_last + error_over+error_over) )
    s.SetBinContent(s.GetNbinsX()+1,0)
    s.SetBinError(s.GetNbinsX()+1,0)

#CHANGE: add other contributions (DY, fake, ...)

TT.GetXaxis().SetTitle("")
TT.GetXaxis().SetNdivisions(505)
TT.GetXaxis().SetLabelFont(42)
TT.GetXaxis().SetLabelSize(0.06)
TT.GetXaxis().SetTitleOffset(1)
TT.GetXaxis().SetTitleSize(0.06)

TT.GetYaxis().SetLabelFont(42)
TT.GetYaxis().SetLabelOffset(0.01)
TT.GetYaxis().SetLabelSize(0.06)
TT.GetYaxis().SetTitleSize(0.075)
TT.GetYaxis().SetTitleOffset(1.2)
TT.GetYaxis().SetMaxDigits(3)
TT.SetTitle("")
TT.GetYaxis().SetTitle("Events/bin")
TT.GetXaxis().SetTitle(f"{parameter_title}")
TT.SetMinimum(0.1)
TT.SetMaximum(1e5)

TT.GetYaxis().SetRangeUser(0.1, 1e7)
TT.GetXaxis().SetRangeUser(0, 400)


#CMS 6-color scheme ["#5790fc", "#f89c20", "#e42536", "#964a8b", "#9c9ca1", "#7a21dd"] 
#CMS 10-color scheme ["#3f90da", "#ffa90e", "#bd1f01", "#94a4a2", "#832db6", "#a96b59", "#e76300", "#b9ac70", "#717581", "#92dadd"]
TT.SetLineColor(ROOT.TColor.GetColor("#5790fc"))
VV.SetLineColor(ROOT.TColor.GetColor("#f89c20"))
DY.SetLineColor(ROOT.TColor.GetColor("#e42536"))
STL.SetLineColor(ROOT.kBlue)

# TT.SetLineColor(1)
# VV.SetLineColor(1)
# DY.SetLineColor(1)
#CHANGE: set the color and style for other contributions
STL.SetLineWidth(3)
TT.SetLineWidth(3)
VV.SetLineWidth(3)
DY.SetLineWidth(3)


#CHANGE: stack the other contributions

errorBand = TT.Clone()
errorBand.Add(VV)
errorBand.Add(DY)
#CHANGE: add the other contributions


errorBand.SetMarkerSize(0)
errorBand.SetFillColor(new_idx)
errorBand.SetFillStyle(3001)
errorBand.SetLineWidth(1)

c.SetFillColor(0)
c.SetBorderMode(0)
c.SetBorderSize(10)
c.SetTickx(1)
c.SetTicky(1)
c.SetLeftMargin(0.18)
c.SetRightMargin(0.05)
c.SetTopMargin(0.102)
c.SetBottomMargin(0.13)
c.SetFrameFillStyle(0)
c.SetFrameLineStyle(0)
c.SetFrameBorderMode(0)
c.SetFrameBorderSize(10)
c.SetLogy()

# errorBand.Draw("e2same")


TT.Draw("histsame")
VV.Draw("histsame")
DY.Draw("histsame")

STL.Draw("histsame")

legende=make_legend()
legende.AddEntry(DY,"Drell-Yan","l")
legende.AddEntry(TT,"t#bar{t}","l")
legende.AddEntry(VV,"WW","l")
legende.AddEntry(STL,"#tilde{#tau}_{L} #sigma=" + str(int(signal_scale)) + " pb","l")
# legende.AddEntry(errorBand,"Uncertainty","f")
legende.Draw()

l1=add_lumi()
l1.Draw("same")
l2=add_CMS()
l2.Draw("same")
l3=add_Preliminary()
l3.Draw("same")


c.Draw()
ROOT.gPad.RedrawAxis()

c.Modified()
c.SaveAs(f"{parameter_name}.png")

