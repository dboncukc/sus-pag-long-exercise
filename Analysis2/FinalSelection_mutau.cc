#include <TH2.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphAsymmErrors.h>
#include "TMultiGraph.h"
#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <utility>
#include <stdio.h>
#include <TF1.h>
#include <TDirectoryFile.h>
#include <TRandom3.h>
#include "TLorentzVector.h"
#include "TString.h"
#include "TLegend.h"
#include "TH1F.h"
#include "TKey.h"
#include "THashList.h"
#include "THStack.h"
#include "TPaveLabel.h"
#include "TFile.h"
#include "TTree.h"
#include "tr_Tree.h"

using namespace std;

int main(int argc, char** argv) {

   int counterA = 0;
   int counterB = 0;
   int counterC = 0;
   int counterD = 0;
   std::string year = *(argv + 1);
   std::string input = *(argv + 2);
   std::string output = *(argv + 3);
   std::string sample = *(argv + 4);
   std::string name = *(argv + 5);
   cout << "Processing: " << name << endl;

   TFile *f_Double = new TFile(input.c_str());
   cout<<"XXXXXXXXXXXXX "<<input.c_str()<<" XXXXXXXXXXXX"<<endl;
   TTree *arbre = (TTree*) f_Double->Get("Events");

   TTree *arbre2 = (TTree*) f_Double->Get("Runs");
   float ngen=0;
   float ngenu=0;
   if (name!="data_obs") {
      Int_t nentries_wtn2 = (Int_t) arbre2->GetEntries();
      arbre2->SetBranchAddress("genEventCount", &genEventCount);
      for (Int_t i = 0; i < nentries_wtn2; i++) {
      arbre2->GetEntry(i);
      ngenu+=genEventCount;
      }
   }
   cout<<"N gen unweighted: "<<ngenu<<endl;

   float weight=1.0; float xs=1.0;
   // Info from https://twiki.cern.ch/twiki/bin/view/CMS/PdmVRun3Analysis
   float lumi2022C=5010.4;
   float lumi2022D=2970.0;
   float lumi2022E=5807.0;
   float lumi2022F=17781.9;
   float lumi2022G=3082.8;
   float lumi2022=34748.6;
   float lumi2022_postEE=lumi2022E+lumi2022F+lumi2022G;
   cout <<"Lumi:" << lumi2022_postEE << endl;

   // CHANGE: compute the weight for each MC sample based on the integrated luminosity (lumi2022_postEE), the number of generated events (ngenu), and the sample cross section
   float TTTo2L2Nu_weight = (lumi2022_postEE * 923.6* 0.1061) / ngenu;
   float WW_weight = ( lumi2022_postEE * 120) / ngenu;
   float DY_weight =  ( lumi2022_postEE * 6345.99) / ngenu;
   float STR_weight = ( lumi2022_postEE * 1.0) / ngenu;
   float STL_weight = ( lumi2022_postEE * 1.0) / ngenu;

   if (name=="TTTo2L2Nu") weight = TTTo2L2Nu_weight;
   if (name=="WW") weight = WW_weight;
   if (name=="DY") weight = DY_weight;
   if (name=="stau_right") weight = STR_weight;
   if (name=="stau_left") weight = STL_weight;


   cout.setf(ios::fixed, ios::floatfield);
   cout.precision(10);

   arbre->SetBranchAddress("nLepCand", &nLepCand);
   arbre->SetBranchAddress("LepCand_id", &LepCand_id);
   arbre->SetBranchAddress("LepCand_pt", &LepCand_pt);
   arbre->SetBranchAddress("LepCand_eta", &LepCand_eta);
   arbre->SetBranchAddress("LepCand_phi", &LepCand_phi);
   arbre->SetBranchAddress("LepCand_tauvse2018", &LepCand_tauvse2018);
   arbre->SetBranchAddress("LepCand_tauvsmu2018", &LepCand_tauvsmu2018);
   arbre->SetBranchAddress("LepCand_tauvsjet2018", &LepCand_tauvsjet2018);
   arbre->SetBranchAddress("LepCand_charge", &LepCand_charge);
   arbre->SetBranchAddress("LepCand_taudm", &LepCand_taudm);
   arbre->SetBranchAddress("LepCand_gen", &LepCand_gen);
   arbre->SetBranchAddress("HLT_IsoMu24", &HLT_IsoMu24);
   arbre->SetBranchAddress("nJets", &nJets);
   if (name != "data_obs"){
      arbre->SetBranchAddress("nbJets", &nbJets);
   }else {
      nbJets = -1;
   }
   arbre->SetBranchAddress("PuppiMET_phi", &PuppiMET_phi);
   arbre->SetBranchAddress("PuppiMET_pt", &PuppiMET_pt);

   arbre->SetBranchAddress("LepCand_dxy", &LepCand_dxy);
   arbre->SetBranchAddress("LepCand_dz", &LepCand_dz);
   
   arbre->SetBranchAddress("LepCand_tauvsjet2018_sf", &LepCand_tauvsjet2018_sf);
   arbre->SetBranchAddress("LepCand_tauvsmu2018_sf", &LepCand_tauvsmu2018_sf);
   arbre->SetBranchAddress("LepCand_muonIso_sf", &LepCand_muonIso_sf);
   arbre->SetBranchAddress("LepCand_muonID_sf", &LepCand_muonID_sf);
   arbre->SetBranchAddress("LepCand_trg_sf", &LepCand_trg_sf);
   arbre->SetBranchAddress("PuppiMET_pt", &PuppiMET_pt);

   TH1F* h_met_pt = new TH1F("h_met_pt","met_pt",50,0,500); h_met_pt->Sumw2();


   TH1F* h_mvis = new TH1F("h_mvis","h_mvis",30,0,300); h_mvis->Sumw2();
   TH1F* h_mvis_anti = new TH1F("h_mvis_anti","h_mvis_anti",30,0,300); h_mvis_anti->Sumw2();

   TH1F* h_mu_phi = new TH1F("mu_phi","mu_phi",50,-4,4); h_mu_phi->Sumw2();
   TH1F* h_tau_phi = new TH1F("tau_phi","tau_phi",50,-4,4); h_tau_phi->Sumw2();

   TH1F* h_mu_eta = new TH1F("mu_eta","mu_eta",50,-4,4); h_mu_eta->Sumw2();
   TH1F* h_tau_eta = new TH1F("tau_eta","tau_eta",50,-4,4); h_tau_eta->Sumw2();

   TH1F* h_mu_pt = new TH1F("mu_pt","mu_pt",50,0,500); h_mu_pt->Sumw2();
   TH1F* h_tau_pt = new TH1F("tau_pt","tau_pt",50,0,500); h_tau_pt->Sumw2();

   TH1F* h_mu_m = new TH1F("mu_m","mu_m",50,0.05,0.15); h_mu_m->Sumw2();
   TH1F* h_tau_m = new TH1F("tau_m","tau_m",50,0.05,0.15); h_tau_m->Sumw2();

   TH1F* h_mu_mt = new TH1F("mu_mt","mu_mt",50,0,300); h_mu_mt->Sumw2();
   TH1F* h_tau_mt = new TH1F("tau_mt","tau_mt",50,0,300); h_tau_mt->Sumw2();
   TH1F* h_mu_tau_mt = new TH1F("mu_tau_mt","mu_tau_mt",50,0,550); h_mu_tau_mt->Sumw2();


   TH1F* h_njets = new TH1F("njets","njets",10,0,10); h_njets->Sumw2();
   TH1F* h_nbjets = new TH1F("nbjets","nbjets",10,0,10); h_nbjets->Sumw2();


   TH1F* h_tau_pt_den = new TH1F("tau_pt_den","tau_pt_den",70,0,200); h_tau_pt_den->Sumw2();
   TH1F* h_tau_pt_num = new TH1F("tau_pt_num","tau_pt_num",70,0,200); h_tau_pt_num->Sumw2();

    // CHANGE: define here other histograms you want to fill and save

   Int_t nentries_wtn = (Int_t) arbre->GetEntries();
   for (Int_t i = 0; i < nentries_wtn; i++) {
	arbre->LoadTree(i);
        arbre->GetEntry(i);
        if (i % 10000 == 0) fprintf(stdout, "\r  Processed events: %8d of %8d ", i, nentries_wtn);
        fflush(stdout);
	
	int mu_index=-1;
	for (int j=0; j<nLepCand; ++j){
	   if (mu_index<0 and LepCand_id[j]==13) mu_index=j;
	}
        int tau_index=-1; float pt_tmp=0;
        for (int j=0; j<nLepCand; ++j){
           if (LepCand_id[j]==15 and LepCand_pt[j]>pt_tmp){
	      tau_index=j; 
	      pt_tmp=LepCand_pt[j];
	   }
        }

	// build and fill the four-vectors of the muon and the tau 
        TLorentzVector my_tau; 
        TLorentzVector my_mu; 
	my_tau.SetPtEtaPhiM(LepCand_pt[tau_index],LepCand_eta[tau_index],LepCand_phi[tau_index],0.1);
	my_mu.SetPtEtaPhiM(LepCand_pt[mu_index],LepCand_eta[mu_index],LepCand_phi[mu_index],0.1);
   
   // CHANGE: apply the selection on the muon and the tau (eta, pt, ID, ...)
   if (my_mu.Pt() < 26) continue;
   if (abs(my_mu.Eta()) > 2.4) continue;
   if (my_tau.Pt() < 30) continue;
   if (abs(my_tau.Eta()) >2.5) continue; 
   if ( LepCand_dxy[mu_index] > 0.1) continue;
   if ( abs(LepCand_dz[mu_index]) > 0.2) continue;

	// CHANGE: apply the selection on the dilepton pair (DR > 0.5, OS, ...)
   if (my_mu.DeltaR(my_tau) < 0.5) continue;

	// CHANGE: apply the trigger selection

   // if (LepCand_tauvsjet2018[tau_index]<5) continue;// medium
   if (LepCand_tauvse2018[tau_index]<2) continue; // VVLooose
   if (LepCand_tauvsmu2018[tau_index]<4) continue; // Tight

	if (name!="data_obs"){
      if( LepCand_gen[tau_index]==0 ) continue;
       // CHANGE: add SFs for tau ID (against jets, electrons, or muons), muon ID, muon iso, and trigger
	}

   if (!HLT_IsoMu24) continue;

	bool is_OS=(LepCand_charge[mu_index]*LepCand_charge[tau_index]<0);
	bool is_iso=(LepCand_tauvsjet2018[tau_index]>=5);

   bool is_a = is_OS && is_iso;
   bool is_b = is_OS && !is_iso;
   bool is_c = !is_OS && !is_iso;
   bool is_d = !is_OS && is_iso;

	float correction = 1.0;
	if (name!="data_obs"){
	   correction = LepCand_tauvsjet2018_sf[tau_index]*LepCand_tauvsmu2018_sf[tau_index]*LepCand_muonIso_sf[mu_index]*LepCand_muonID_sf[mu_index]*LepCand_trg_sf[mu_index];
       // CHANGE: add SFs for tau ID (against jets, electrons, or muons), muon ID, muon iso, and trigger
	}

	// fill the signal region histogram
	if (is_a){
      counterA++;
      h_mvis->Fill((my_mu+my_tau).M(),weight*correction);
      h_mu_tau_mt->Fill((my_mu+my_tau).Mt(),weight*correction);
      TLorentzVector my_met;
      my_met.SetPtEtaPhiM(PuppiMET_pt,0,PuppiMET_phi,0);

      h_mu_mt->Fill((my_mu + my_met).Mt(),weight*correction);
      h_tau_mt->Fill((my_tau + my_met).Mt(),weight*correction);

      h_mu_phi->Fill(my_mu.Phi(),weight*correction);
      h_tau_phi->Fill(my_tau.Phi(),weight*correction);
      h_mu_eta->Fill(my_mu.Eta(),weight*correction);
      h_tau_eta->Fill(my_tau.Eta(),weight*correction);
      h_mu_pt->Fill(my_mu.Pt(),weight*correction);
      h_tau_pt->Fill(my_tau.Pt(),weight*correction);
      h_mu_m->Fill(my_mu.M(),weight*correction);
      h_tau_m->Fill(my_tau.M(),weight*correction);

      h_njets->Fill(nJets,weight*correction);
      h_nbjets->Fill(nbJets,weight*correction);

      h_met_pt->Fill(PuppiMET_pt,weight*correction);
      // cout << "nJets: " << nJets << endl;
   }
   
   if (is_c){
      counterC++;
      h_tau_pt_den->Fill(LepCand_pt[tau_index],weight*correction);
   }
   if (is_d){
      counterD++;
      h_tau_pt_num->Fill(LepCand_pt[tau_index],weight*correction);
   }
	// fill the anti-isolated histogram to estimate the fake background
	float fr=  1.0 ;  //CHANGE Calculate the tau fake rate and replace this value

	if (is_b){
      counterB++;
      float tau_pt = LepCand_pt[tau_index];

      // 30-40 GeV: 0.077014916 
      // 40-50 GeV: 0.085376754
      // 50-60 GeV: 0.095707625
      if (tau_pt < 40){
         fr = 0.077014916;
      }else if (tau_pt < 50){
         fr =  0.085376754;
      }else if (tau_pt < 60){
         fr = 0.095707625;
      }else{
         fr = 0.1;
      }
      h_mvis_anti->Fill((my_mu+my_tau).M(),weight*fr*correction);
   } 

	// CHANGE: fill the histograms needed to calculate the fake rate
   
   } // end of loop over events

   cout << endl << "..::Counters::.." << endl;
   cout << "A: " << counterA << endl;
   cout << "B: " << counterB << endl;
   cout << "C: " << counterC << endl;
   cout << "D: " << counterD << endl;
   cout << "Total: " << counterA + counterB + counterC + counterD << endl;


   TFile *fout = TFile::Open(output.c_str(), "RECREATE");
   fout->cd();

   TDirectory *dir_parameters =fout->mkdir("parameters");
   dir_parameters->cd();
   // You can store the histogram directly in the root file...

h_mu_phi->SetName((std::string(h_mu_phi->GetName()) + "_" + name).c_str());
h_mu_phi->Write();

h_tau_phi->SetName((std::string(h_tau_phi->GetName()) + "_" + name).c_str());
h_tau_phi->Write();

h_mu_eta->SetName((std::string(h_mu_eta->GetName()) + "_" + name).c_str());
h_mu_eta->Write();

h_tau_eta->SetName((std::string(h_tau_eta->GetName()) + "_" + name).c_str());
h_tau_eta->Write();

h_mu_pt->SetName((std::string(h_mu_pt->GetName()) + "_" + name).c_str());
h_mu_pt->Write();

h_tau_pt->SetName((std::string(h_tau_pt->GetName()) + "_" + name).c_str());
h_tau_pt->Write();

h_mu_m->SetName((std::string(h_mu_m->GetName()) + "_" + name).c_str());
h_mu_m->Write();

h_tau_m->SetName((std::string(h_tau_m->GetName()) + "_" + name).c_str());
h_tau_m->Write();

h_mvis->SetName((std::string(h_mvis->GetName()) + "_" + name).c_str());
h_mvis->Write();

h_mu_tau_mt->SetName((std::string(h_mu_tau_mt->GetName()) + "_" + name).c_str());
h_mu_tau_mt->Write();

h_mu_mt->SetName((std::string(h_mu_mt->GetName()) + "_" + name).c_str());
h_mu_mt->Write();

h_tau_mt->SetName((std::string(h_tau_mt->GetName()) + "_" + name).c_str());
h_tau_mt->Write();

h_njets->SetName((std::string(h_njets->GetName()) + "_" + name).c_str());
h_njets->Write();

h_tau_pt_den->SetName((std::string(h_tau_pt_den->GetName()) + "_" + name).c_str());
h_tau_pt_den->Write();

h_tau_pt_num->SetName((std::string(h_tau_pt_num->GetName()) + "_" + name).c_str());
h_tau_pt_num->Write();

h_met_pt->SetName((std::string(h_met_pt->GetName()) + "_" + name).c_str());
h_met_pt->Write();

h_nbjets->SetName((std::string(h_nbjets->GetName()) + "_" + name).c_str());
h_nbjets->Write();
    
    
    // or in a directory (necessary for the statistical analysis later)

    TDirectory *dir_OSiso =fout->mkdir("OSiso");
    dir_OSiso->cd();
    h_mvis->SetName(name.c_str());
    h_mvis->Write();

    TDirectory *dir_OSanti =fout->mkdir("OSanti");
    dir_OSanti->cd();
    h_mvis_anti->SetName(name.c_str());
    h_mvis_anti->Write();

    // CHANGE: save the other histograms you have filled

    fout->Close();
} 

